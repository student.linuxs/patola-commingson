<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex, follow">
  <meta name="description" content="patola.id" />
  <meta name="next-head-count" content="8" />
  <meta name="keywords" content="patola.id">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="HandheldFriendly" content="true" />
  <meta name="apple-touch-fullscreen" content="yes" />

  <!-- favicon -->
  <link rel="shortcut icon" href="img/favicon.ico">
  <title>patola.id - commingsoon</title>
  <link rel="stylesheet" href="style.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Madimi+One&display=swap" rel="stylesheet">
</head>

<body>

  <div class="hero">
    <div class="info bounce-1">
      <div class="coverh1">
        <h1>OUR WEBSITE IS UNDER CONSTRUCTION.</h1>
        <div class="img">
          <img class="logofix" src="patola.png" alt="">
        </div>

      </div>


    </div>
    <div class="topcloud">
      <img src="cloud.png" alt="cloud" class="cloud">
    </div>
    <div class="ocean">

      <img src="water.png" alt="sea" class="water">
      <div class="boat">
        <img src="ship.png" alt="">
      </div>
    </div>
  </div>
</body>

</html>